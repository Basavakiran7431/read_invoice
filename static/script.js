(function() {
    var files = [];
    var filenames = [];
    document.getElementById('formFile').addEventListener('change', function(event) {
        files = event.target.files;
        if (!document.getElementById('res-list').classList.contains('d-none')) {
            document.getElementById('res-list').classList.add('d-none');
        }
    });
    document.getElementById('uploadbutton').addEventListener('click', validate);
    $('.preview-images').click(previewImage)

    var sampleString = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
    var samplejson = {
        "company": "swn ldamhap sdan bhd",
        "date": "05/002/2018",
        "address": "lot 13 jalan ipoh kg batu 30ulu yam lama 14300 b7g kali selangor.",
        "total": "73"
    };

    function loader(load) {
        const spinner = document.getElementById('loader');
        if (load) {
            spinner.classList.remove('d-none');
            return;
        }
        if (!spinner.classList.contains('d-none')) {
            spinner.classList.add('d-none');
        }
    }

    function loadPreview(load) {
        const spinner = document.getElementById('image-preview-loader');
        const dataCard = document.getElementById('preview-image-data');
        if (load) {
            spinner.classList.remove('d-none');
            if (!dataCard.classList.contains('d-none')) {
                dataCard.classList.add('d-none');
            }
            return;
        }
        if (!spinner.classList.contains('d-none')) {
            spinner.classList.add('d-none');
        }
    }

    function extractNames() {
        filenames = [];
        for (let index = 0; index < files.length; index++) {
            filenames.push(files[index].name);
        }
    }

    function imageToURL(image, index) {
        var reader = new FileReader();
        reader.onload = function(e) {
            var list = document.getElementById('res-list');
            list.children[index].children[0].src = e.target.result;
        }
        reader.readAsDataURL(image);
    }

    function renderData(res) {
        console.log(files);
        var list = document.getElementById('res-list');
        list.classList.remove('d-none');
        while (list.firstChild) {
            list.removeChild(list.lastChild);
        }
        for (let index = 0; index < res.files.length; index++) {
            const dataCard = document.createElement('div');
            const img = document.createElement('img');
            const p = document.createElement('p');
            const ul = document.createElement('ul');

            dataCard.classList.add('data-card');
            p.classList.add('data-text');

            img.alt = res.files[index];
            const json_res = JSON.parse(JSON.stringify(res.data[index]));
            for (const [key, value] of Object.entries(json_res)) {
                const li = document.createElement('li');
                const strong = document.createElement('strong');
                const span = document.createElement('span');

                strong.innerText = key + ' : ';
                span.innerText = value;

                li.append(strong);
                li.append(span);
                ul.append(li);
            }
            p.append(ul);
            dataCard.append(img);
            dataCard.append(p);
            list.append(dataCard);

            const imageIndex = filenames.findIndex(ob => ob.toLowerCase() == res.files[index].toLowerCase());
            imageToURL(files[imageIndex], index);
        }
    }

    function sendData() {
        loader(true);
        let formData = new FormData();
        for (let index = 0; index < files.length; index++) {
            formData.append('file', files[index]);
        }

        $.ajax({
            type: 'POST',
            url: '/extract',
            data: formData,
            contentType: false,
            cache: false,
            processData: false,
            success: function(res) {
                renderData(res);
                loader(false);
            },
            error: function(req, status, err) {
                console.log(req, status, err);
                loader(false);
            }
        });
    }

    function renderPreviewData(res) {
        const dataCard = document.getElementById('preview-image-data');
        dataCard.classList.remove('d-none');
        loadPreview(false);
        while (dataCard.firstChild) {
            dataCard.removeChild(dataCard.lastChild);
        }
        const ul = document.createElement('ul');
        const json_res = JSON.parse(JSON.stringify(res.data[0]));
        for (const [key, value] of Object.entries(json_res)) {
            const li = document.createElement('li');
            const strong = document.createElement('strong');
            const span = document.createElement('span');

            strong.innerText = key + ' : ';
            span.innerText = value;

            li.append(strong);
            li.append(span);
            ul.append(li);
        }
        dataCard.append(ul);
    }

    function sendPreviewImage(filename) {
        console.log('called');
        loadPreview(true);
        const jsondata = { file: filename };
        $.ajax({
            type: 'POST',
            url: '/preview',
            data: JSON.stringify(jsondata),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            cache: false,
            success: function(res) {
                renderPreviewData(res);
                loadPreview(false);
            },
            error: function(req, status, err) {
                console.log(req, status, err);
                loadPreview(false);
            }
        });
    }

    function validate() {
        if (!document.getElementById('res-list').classList.contains('d-none')) {
            document.getElementById('res-list').classList.add('d-none');
        }
        const alertAck = document.getElementById('alert-ack');
        if (files && files.length == 0) {
            alertAck.classList.remove('d-none');
            alertAck.innerText = "Please select file(s)";
            return;
        }
        if (!alertAck.classList.contains('d-none')) {
            alertAck.classList.add('d-none');
        }
        var filenames = [];
        for (let index = 0; index < files.length; index++) {
            let type = files[index].name.toLowerCase().split('.')[files[index].name.toLowerCase().split('.').length - 1];
            if (type != 'pdf' && type != 'png' && type != 'jpg' && type != 'jpeg') {
                alertAck.classList.remove('d-none');
                alertAck.innerText = "unsopperted file types";
                return;
            }
            filenames.push(files[index].name);
        }
        if (!alertAck.classList.contains('d-none')) {
            alertAck.classList.add('d-none');
        }
        extractNames();
        sendData();
    }

    function previewImage(event) {
        $('.preview-images').removeClass('image-active');
        $(event.target).addClass('image-active');
        $('#preview-image').attr('src', event.target.src);
        sendPreviewImage(event.target.src.split('/').pop());
    }

    function previewFirstImage() {
        const firstImageURL = $('.preview-images')[0].src;
        $('.preview-images').removeClass('image-active');
        $('.preview-images').first().addClass('image-active');
        $('#preview-image').attr('src', firstImageURL);
        sendPreviewImage(firstImageURL.split('/').pop());
    }

    previewFirstImage();
})();